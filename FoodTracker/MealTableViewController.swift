//
//  MealTableViewController.swift
//  FoodTracker
//
//  Created by Md. Kamrul Hasan on 1/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit
import os.log

class MealTableViewController: UITableViewController {
    
    // MARK: Properties
    var meals = [Meal]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Use the edit button item provided by the table view controller.
        navigationItem.leftBarButtonItem = editButtonItem
        
        // load any saved meals, otherwise load sample data
        if let savedMeals = loadMeals(){
            meals += savedMeals
        } else {
        // Load the sample data
            loadSampleMeals()
            
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meals.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        // own methods used
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MealTableViewCell
        // Fetches the appropriate meal for the data source layout.
        let meal = meals[indexPath.row]
        cell.nameLabel.text = meal.name
        cell.photoImageView.image = meal.photo
        cell.ratingControl.rating = meal.rating
        
        return cell
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            meals.remove(at: indexPath.row)
            saveMeals()
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch(segue.identifier ?? ""){
        case "AddItem":
            os_log("Adding a new meal.", log: OSLog.default, type: .debug)
        case "ShowDetail":
            guard let mealDetailViewController = segue.destination as? ViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedMealCell = sender as? MealTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedMealCell) else {
                fatalError("the selected cell is not being displayed by the table")
            }
            
            let selectedMeal = meals[indexPath.row]
            mealDetailViewController.meal = selectedMeal
            
        default:
            fatalError("Unexpected segue identifier: \(String(describing: segue.identifier))")
        }
    }
    
    // MARK: Actions
    @IBAction func unwindToMealList(sender: UIStoryboardSegue){
        if let sourceViewController = sender.source as? ViewController, let meal = sourceViewController.meal{
            if let selectedIndexPath = tableView.indexPathForSelectedRow{
                // Update an existing meal
                meals[selectedIndexPath.row] = meal
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            } else {
                // Add a new meal
                let newIndexPath = IndexPath(row: meals.count, section: 0)
                meals.append(meal)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
            // save the meals
            saveMeals()
        }
    }
    
    // MARK: Private Methods
    private func loadSampleMeals(){
        let photo1 = UIImage(named: "burger")
        let photo2 = UIImage(named: "chicken mushroom")
        let photo3 = UIImage(named: "Coachella")
        let photo4 = UIImage(named: "GrabFood")
        let photo5 = UIImage(named: "noodles")
        let photo6 = UIImage(named: "ramo")
        
        guard let Meal1 = Meal(name: "Burger Boom", photo: photo1, rating: 4) else {
            fatalError(" Unable to instantiate Meal 1")
        }
        
        guard let Meal2 = Meal(name: "Chicken Mushroom", photo: photo2, rating: 5) else {
            fatalError(" Unable to instantiate Meal 1")
        }
        
        guard let Meal3 = Meal(name: "Coachella", photo: photo3, rating: 3) else {
            fatalError(" Unable to instantiate Meal 1")
        }
        guard let Meal4 = Meal(name: "GrabFood", photo: photo4, rating: 2) else {
            fatalError(" Unable to instantiate Meal 1")
        }
        guard let Meal5 = Meal(name: "Noodles", photo: photo5, rating: 1) else {
            fatalError(" Unable to instantiate Meal 1")
        }
        guard let Meal6 = Meal(name: "Ramo oh...", photo: photo6, rating: 4) else {
            fatalError(" Unable to instantiate Meal 4.5")
        }
        
        meals += [Meal1,Meal2,Meal3,Meal4,Meal5, Meal6]
    }

    // MARK: Save data -- updated methods --
    private func saveMeals(){
        let fullPath = getDocumentsDirectory().appendingPathComponent("meals")
        
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: meals, requiringSecureCoding: false)
            try data.write(to: fullPath)
            os_log("Meals successfully saved.", log: OSLog.default, type: .debug)
        } catch {
            os_log("Failed to save meals...", log: OSLog.default, type: .error)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    private func loadMeals() -> [Meal]? {
        let fullPath = getDocumentsDirectory().appendingPathComponent("meals")
        if let nsData = NSData(contentsOf: fullPath) {
            do {
                
                let data = Data(referencing:nsData)
                
                if let loadedMeals = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? Array<Meal> {
                    return loadedMeals
                }
            } catch {
                print("Couldn't read file.")
                return nil
            }
        }
        return nil
    }

}
